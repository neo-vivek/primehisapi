﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace HMSAPI.Core
{
    public class Cipher
    {

        public static string Encrypt(string toEncrypt)
        {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
                //byte[] toEncryptArray = UTF32Encoding.UTF32.GetBytes(toEncrypt);

                // Get the key from config file
                string key = "@#$%^_+!@#$%^?:;#@";
                //System.Windows.Forms.MessageBox.Show(key);

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();


                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception ex)
            {
                return toEncrypt;

            }
        }

        public static string Decrypt(string cipherString)
        {
            try
            {
                byte[] keyArray;
                //byte[] toEncryptArray = Convert.FromBase64String(cipherString);
                byte[] toEncryptArray = Convert.FromBase64String(cipherString);
                //Get your key from config file to open the lock!
                string key = "@#$%^_+!@#$%^?:;#@";

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                tdes.Clear();
                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception ex)
            {
                return cipherString;

            }
        }
    }
}
