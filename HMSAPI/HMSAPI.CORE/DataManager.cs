﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace HMSAPI.Core
{
    public class DataManager
    {
        public static string CompName { get; set; }
        public static string ConnectionString { get; set; }


        #region Dataset 
        public DataSet GetDataSet(string spname, List<ViewParam> par, string CompName = null)
        {
            SqlConnection con = New_connection(CompName);
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(spname, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;
            foreach (ViewParam vp in par)
            {
                cmd.Parameters.AddWithValue(vp.Name, vp.Value);
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataTable Get(string query, string CompName = null)
        {
            SqlConnection con = New_connection(CompName);
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public int Update(string query, string CompName = null)
        {
            SqlConnection con = New_connection(CompName);
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.CommandTimeout = 0;
            int Count = 0;
            con.Open();
            Count = cmd.ExecuteNonQuery();
            con.Close();
            return Count;
        }
        public string GetValue(string query, string CompName = null)
        {
            SqlConnection con = New_connection(CompName);
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }
        public SqlConnection New_connection(string CompName = null)
        {
            if (string.IsNullOrEmpty(CompName))
            {
                CompName = "ind";
            }
            var configurationBuilder = new ConfigurationBuilder();
            string path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var conStrEnc = configurationBuilder.Build().GetSection("ConnectionStrings:"+ CompName).Value;
            ConnectionString = Cipher.Decrypt(conStrEnc);
            string source = ConnectionString + ";" + "Connect Timeout=0";
            SqlConnection conn = new SqlConnection(source);
            return conn;
        }

        //static DataManager()
        //{
        //    var configurationBuilder = new ConfigurationBuilder();
        //    string path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
        //    configurationBuilder.AddJsonFile(path, false);
        //    var conStrEnc = configurationBuilder.Build().GetSection("ConnectionStrings:connectionStrings").Value;
        //    ConnectionString = Cipher.Decrypt(conStrEnc);
        //}

        #endregion

    }
    public class ViewParam
    {
        public string Name;
        public object Value;

    }

}