﻿namespace HMSAPI.Core
{
    public enum MessageType
    {
        Warning = 0,
        Success = 1,
        Danger = 2,
        Info = 3,
    }

    public enum ModalSize
    {
        Small,
        Medium,
        Large
    }
   
}
