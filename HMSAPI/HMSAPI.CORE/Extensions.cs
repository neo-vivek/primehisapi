﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Reflection;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.IO;
using System.Xml;

namespace HMSAPI.Core
{
    public static class Extensions
    {
        #region "Dictionary"

        public static void AddOrReplace(this IDictionary<string, object> DICT, string key, object value)
        {
            if (DICT.ContainsKey(key))
                DICT[key] = value;
            else
                DICT.Add(key, value);
        }

        public static dynamic GetObjectOrDefault(this IDictionary<string, object> DICT, string key)
        {
            if (DICT.ContainsKey(key))
                return DICT[key];
            else
                return null;
        }

        public static T GetObjectOrDefault<T>(this IDictionary<string, object> DICT, string key)
        {
            if (DICT.ContainsKey(key))
                return (T)Convert.ChangeType(DICT[key], typeof(T));
            else
                return default(T);
        }

        #endregion

        #region "String"

        public static string ToSelfURL(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            string outputStr = text.Trim().Replace(":", "").Replace("&", "").Replace(" ", "-").Replace("'", "").Replace(",", "").Replace("(", "").Replace(")", "").Replace("--", "").Replace(".", "");
            return Regex.Replace(outputStr.Trim().ToLower().Replace("--", ""), "[^a-zA-Z0-9_-]+", "", RegexOptions.Compiled);
        }

        public static string TrimLength(this string input, int length, bool Incomplete = true)
        {
            if (String.IsNullOrEmpty(input)) { return String.Empty; }
            return input.Length > length ? String.Concat(input.Substring(0, length), Incomplete ? "..." : "") : input;
        }

        public static string ToTitle(this string input)
        {
            return String.IsNullOrEmpty(input) ? String.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
        }

        public static bool ContainsAny(this string input, params string[] values)
        {
            return String.IsNullOrEmpty(input) ? false : values.Any(S => input.Contains(S));
        }

        public static DateTime ToDateTime(this string str, bool isWithTime = false)
        {
            if (string.IsNullOrWhiteSpace(str))
                return DateTime.Now;

            string[] formats = { "dd/MM/yyyy", "d/MM/yyyy", "dd/M/yyyy", "dd/MM/yyyy h:mm:ss tt", "d/MM/yyyy h:mm:ss tt", "dd/M/yyyy h:mm:ss tt", "yyyy-MM-dd", "yyyy-M-dd", "yyyy-MM-d", "yyyy-MM-dd h:mm:ss tt", "yyyy-M-dd h:mm:ss tt", "yyyy-MM-d h:mm:ss tt", "dd-MM-yyyy", "d-MM-yyyy", "dd-M-yyyy", "dd-MM-yyyy h:mm:ss tt", "d-MM-yyyy h:mm:ss tt", "dd-M-yyyy h:mm:ss tt", "yyyy/MM/dd", "yyyy/M/dd", "yyyy/MM/d", "yyyy/MM/dd  h:mm:ss tt", "yyyy/M/dd  h:mm:ss tt", "yyyy/MM/d  h:mm:ss tt", "d/M/yyyy h:mm:ss tt", "M/dd/yyyy h:mm:ss tt", "MM/dd/yyyy h:mm:ss tt", "MM/d/yyyy h:mm:ss tt", "M/dd/yyyy", "MM/dd/yyyy", "MM/d/yyyy", "yyyyMMdd", "d/M/yy" };
            if (isWithTime)
            {
                return DateTime.ParseExact(str, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }

            return DateTime.ParseExact(str, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        #endregion

        #region "Collection"

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source != null && source.Count() >= 0)
            {
                foreach (T element in source)
                {
                    action(element);
                }
            }
        }

        public static bool IsNotNullAndNotEmpty<T>(this ICollection<T> source)
        {
            return source != null && source.Count() > 0;
        }

        #endregion

        #region "DateTime"

        public static string ToFormatString(this DateTime? date)
        {
            if (!date.HasValue)
                return string.Empty;

            return date.Value.ToString("dd MMM, yyyy");
        }

        public static string ToFormatString(this DateTime date)
        {
            return date.ToString("dd MMM, yyyy");
        }

        public static string ToFormatCustomString(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }

        public static string ToFormatCustomString(this DateTime date, string dateformat)
        {
            return date.ToString(dateformat);
        }

        public static string ToFormatCustomString(this DateTime? date)
        {
            if (!date.HasValue)
                return string.Empty;

            return date.Value.ToString("dd/MM/yyyy");
        }

        public static string ToDateTimeString(this DateTime? date)
        {
            if (!date.HasValue)
                return string.Empty;

            return date.Value.ToString("dd/MM/yyyy HH:mm");
        }

        public static string IsoFormatTime(this DateTimeOffset time)
        {
            return time.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

        public static string ToFinancialYear(this DateTime dateTime)
        {
            return "31/12/" + dateTime.Year;
        }

        public static DateTime StringToDateFormat(this string date)
        {
            string[] formats = { "dd/MM/yyyy", "d/MM/yyyy", "dd/M/yyyy", "dd/MM/yyyy h:mm:ss tt", "d/MM/yyyy h:mm:ss tt", "dd/M/yyyy h:mm:ss tt", "yyyy-MM-dd", "yyyy-M-dd", "yyyy-MM-d", "yyyy-MM-dd h:mm:ss tt", "yyyy-M-dd h:mm:ss tt", "yyyy-MM-d h:mm:ss tt", "dd-MM-yyyy", "d-MM-yyyy", "dd-M-yyyy", "dd-MM-yyyy h:mm:ss tt", "d-MM-yyyy h:mm:ss tt", "dd-M-yyyy h:mm:ss tt", "yyyy/MM/dd", "yyyy/M/dd", "yyyy/MM/d", "yyyy/MM/dd  h:mm:ss tt", "yyyy/M/dd  h:mm:ss tt", "yyyy/MM/d  h:mm:ss tt", "d/M/yyyy h:mm:ss tt", "M/dd/yyyy h:mm:ss tt", "MM/dd/yyyy h:mm:ss tt", "MM/d/yyyy h:mm:ss tt", "M/dd/yyyy", "MM/dd/yyyy", "MM/d/yyyy", "yyyyMMdd", "d/M/yy" };

            return DateTime.ParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
        }



        #endregion


        #region XmlExtensions
        public static XDocument ToXML1(this object obj, string defaultNameSpace = "")
        {
            XmlSerializer x = new XmlSerializer(obj.GetType());
            XDocument XDoc = new XDocument();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            if (defaultNameSpace != "")
            {
                //  Add lib namespace with empty prefix
                ns.Add("", defaultNameSpace);
            }
            using (var writer = XDoc.CreateWriter())
            {
                x.Serialize(writer, obj, ns);
            }

            if (XDoc != null)
                return XDoc;

            return null;
        }
        public static string ToXML<T>(this T xmlObject)
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(xmlObject.GetType());
            serializer.Serialize(stringwriter, xmlObject);
            return stringwriter.ToString();
        }

        public static T ToObject<T>(this string xml)
        {
            var stringReader = new System.IO.StringReader(xml);
            var serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(stringReader);
        }

        public static void SaveXMLFromObject(XDocument obj, String XMLPath, String SearchType)
        {
            if (!Directory.Exists(XMLPath))
            {
                Directory.CreateDirectory(XMLPath);
            }

            if (obj != null)
                obj.Save(XMLPath + "/" + SearchType + ".xml");
        }
        public static void SaveJSONFromObject(string obj, String Path, String SearchType)
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            if (obj != null)
            {
                // string json = .SerializeObject(obj.ToArray());

                //write string to file
                System.IO.File.WriteAllText(Path + "/" + SearchType + ".json", obj);
            }
            //  obj.Save(Path + "/" + SearchType + ".json");
        }



        public static T XmlDeserializeByReader<T>(this string xmlFileName) where T : new()
        {
            StreamReader reader = new StreamReader(xmlFileName);
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.CloseInput = true;
            settings.IgnoreWhitespace = true;

            T xmlData;
            using (XmlReader xmlReader = XmlReader.Create(reader, settings))
            {
                xmlData = (T)serializer.Deserialize(xmlReader);
            }
            return xmlData;
        }


        #endregion

        /* Get Enum Description by Enum value */
        public static string GetEnumDescription(this Enum value)
        {
            var type = value.GetType();
            var memInfo = type.GetMember(value.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            var description = ((DescriptionAttribute)attributes[0]).Description;
            return description;
        }





        public static string PriceFormat(decimal value)
        {
            var s = string.Format("{0:0.00}", value);

            if (s.EndsWith("00"))
            {
                return ((int)value).ToString();
            }
            else
            {
                return s;
            }
        }


    }
}