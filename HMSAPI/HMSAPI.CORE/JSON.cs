﻿using System.Data;
using System.Text;

namespace HMSAPI.Core
{
    public class JSON
    {
        public static string ConverDatasetToJson(DataSet dsDownloadJson)
        {
            StringBuilder Sb = new StringBuilder();
            Sb.Append("{");
            foreach (DataTable dtDownloadJson in dsDownloadJson.Tables)
            {
                string HeadStr = string.Empty;
                Sb.Append("\"" + dtDownloadJson.TableName + "1\": [");
                for (int j = 0; j < dtDownloadJson.Rows.Count; j++)
                {
                    string TempStr = HeadStr;
                    Sb.Append("{");
                    for (int i = 0; i < dtDownloadJson.Columns.Count; i++)
                    {
                        string caption = dtDownloadJson.Columns[i].Caption;
                        Sb.Append("\"" + caption + "\" : " + Enquote(dtDownloadJson.Rows[j][i].ToString()) + ",");
                    }
                    Sb.Append(TempStr + "},");
                }
                Sb.Append("],");
            }
            Sb.Append("}");
            return Sb.ToString();
        }

        public static string ConverDatasetToJson(DataSet dsDownloadJson, string TableName)
        {
            StringBuilder Sb = new StringBuilder();
            Sb.Append("{\"ok\":1,");
            foreach (DataTable dtDownloadJson in dsDownloadJson.Tables)
            {
                string HeadStr = string.Empty;
                Sb.Append("\"" + TableName + "\": [");
                for (int j = 0; j < dtDownloadJson.Rows.Count; j++)
                {
                    string TempStr = HeadStr;
                    Sb.Append("{");
                    for (int i = 0; i < dtDownloadJson.Columns.Count; i++)
                    {
                        string caption = dtDownloadJson.Columns[i].Caption;
                        Sb.Append("\"" + caption + "\" : " + Enquote(dtDownloadJson.Rows[j][i].ToString()) + ",");
                    }
                    Sb.Append(TempStr + "},");
                }
                Sb.Append("],");
            }
            Sb.Append("\"ERRORS\":[]}");
            return Sb.ToString();
        }

        public static string Enquote(string s)
        {
            if (s == null || s.Length == 0)
            {
                return "\"\"";
            }
            char c;
            int i;
            int len = s.Length;
            StringBuilder sb = new StringBuilder(len + 4);
            string t;

            sb.Append('"');
            for (i = 0; i < len; i += 1)
            {
                c = s[i];
                if ((c == '\\') || (c == '"') || (c == '>'))
                {
                    sb.Append('\\');
                    sb.Append(c);
                }
                else if (c == '\b')
                    sb.Append("\\b");
                else if (c == '\t')
                    sb.Append("\\t");
                else if (c == '\n')
                    sb.Append("\\n");
                else if (c == '\f')
                    sb.Append("\\f");
                else if (c == '\r')
                    sb.Append("\\r");
                else
                {
                    if (c < ' ')
                    {
                        //t = "000" + Integer.toHexString(c);
                        string tmp = new string(c, 1);
                        t = "000" + int.Parse(tmp, System.Globalization.NumberStyles.HexNumber);
                        sb.Append("\\u" + t.Substring(t.Length - 4));
                    }
                    else
                    {
                        sb.Append(c);
                    }
                }
            }
            sb.Append('"');
            return sb.ToString();
        }

    }
   


}
