﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMSAPI.Core
{
    public class TResponse
    {
        public int ResponseCode { get; set; }
        public bool ResponseStatus { get; set; }
        public string ResponseMessage { get; set; }
        public object ResponsePacket { get; set; }
    }
    public static class ResponseCode
    {
        public static int OK = 200;
        public static int Error = 201;
        public static int CallTokenError = 202;
        public static int AuthFail = 203;
    }
    public static class ResponseMessage
    {
        public static string Unauthorised = "Unauthorised access detected";
        public static string Save = "Record has been created successfully !!";
        public static string Update = "Record has been updated successfully !!";
        public static string Delete = "Record has been deleted !!";


    }
}