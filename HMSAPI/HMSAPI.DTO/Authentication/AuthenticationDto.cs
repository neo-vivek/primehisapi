﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMSAPI.DTO
{
    public class APPIRegDTO
    {
        public string CompName { get; set; }
        public string ConnectionString { get; set; }
        public string AuthPassword { get; set; }
        public int CompId { get; set; }
        public string Key { get; set; }
        public string VerifyKey { get; set; }
        public string Sno { get; set; }
        public string Rkey { get; set; }
    }





    public class AuthenticationDto
    {
        public string CompName { get; set; }
        public string ConnectionString { get; set; }
        public string AuthPswd { get; set; }
        public int CompId { get; set; }
        public string Key { get; set; }
    }


}
