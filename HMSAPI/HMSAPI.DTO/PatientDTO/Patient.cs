﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HMSAPI.DTO
{
   public  class PatientDTO
    {
        public int ptmast_kid { get; set; }
        public string ptmast_date { get; set; }
        public string ptitle_ename { get; set; }
        public string ptmast_ename { get; set; }
        public string ptmast_sex { get; set; }
        public string gender { get; set; }
        public string Age { get; set; }
        public string ptmast_dob { get; set; }
        public string pct_ename { get; set; }
    }
}