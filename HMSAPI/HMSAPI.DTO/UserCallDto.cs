﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HMSAPI.DTO
{
    public class UserLoginDto
    {

        public string MobileNo { get; set; }
        public string MId { get; set; }
    }


    public class LoginResponseDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string AutKey { get; set; }
        public string Token { get; set; }
    }

    public class UserCallDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public int Status { get; set; }
        
    }

    public class UserCallLogDto
    {
        public int UserId { get; set; }
        public string MobileNo { get; set; }
        public string Type { get; set; }
        public int Duration { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
    }


}
