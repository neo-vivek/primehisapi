﻿using HMSAPI.Core;
using HMSAPI.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HMSAPI.Service
{
    public class AuthenticationService : IAuthenticationService
    {
        public AuthenticationDto Authentication(string compName)
        {
            AuthenticationDto _model = new AuthenticationDto();
            DataManager dm = new DataManager();
            if (!String.IsNullOrEmpty(compName))
            {
                DataTable dt = dm.Get("select APIRegVerify_kid,APIRegVerify_CompName,APIRegVerify_AuthPassword,APIRegVerify_Key from APIRegVerify where APIRegVerify_CompName='" + compName + "'and APIRegVerify_Status=1 ");
                if (dt != null && dt.Rows.Count > 0)
                {
                    int apphopId = Convert.ToInt32(dt.Rows[0]["AppHosp_kid"].ToString());
                    if (apphopId != 0)
                    {
                        _model = dt.AsEnumerable().Select(x => new AuthenticationDto
                        {
                            AuthPswd = x.Field<string>("APIRegVerify_AuthPassword"),
                            CompId = x.Field<int>("APIRegVerify_kid"),
                            CompName = x.Field<string>("APIRegVerify_CompName"),
                            Key = x.Field<string>("APIRegVerify_Key"),
                        }).FirstOrDefault();
                    }
                }
                else
                {
                    return _model;
                }
            }

            return _model;
        }

        public List<AuthenticationDto> AuthenticationData()
        {
            List<AuthenticationDto> datalist = new List<AuthenticationDto>();
            DataManager dm = new DataManager();
            DataTable dt = dm.Get("select APIRegVerify_kid,APIRegVerify_CompName,APIRegVerify_AuthPassword,APIRegVerify_Key from APIRegVerify where  APIRegVerify_Status=1");
            if (dt != null)
            {
                datalist = dt.AsEnumerable().Select(x => new AuthenticationDto
                {
                    AuthPswd = x.Field<string>("APIRegVerify_AuthPassword"),
                    CompName = x.Field<string>("APIRegVerify_CompName"),
                    Key = x.Field<string>("APIRegVerify_kid"),
                }).ToList();
            }
            return datalist;
        }



    }
}
