﻿using HMSAPI.Core;
using HMSAPI.DTO;
using System;
using System.Collections.Generic;


namespace HMSAPI.Service
{
   public interface IAuthenticationService
    {
        AuthenticationDto Authentication(string compName);
        List<AuthenticationDto> AuthenticationData();
    }
}
