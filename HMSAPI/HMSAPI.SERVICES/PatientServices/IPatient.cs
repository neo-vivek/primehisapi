﻿using System;
using System.Collections.Generic;
using System.Text;
using HMSAPI.DTO;

namespace HMSAPI.Service
{
    public interface IPatient
    {
        List<PatientDTO> GetPatient();
    }
}
