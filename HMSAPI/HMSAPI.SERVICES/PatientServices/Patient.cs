﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HMSAPI.Core;
using HMSAPI.DTO;

namespace HMSAPI.Service
{
    public class Patient : IPatient
    {
        DataManager DM = new DataManager();
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        List<ViewParam> _lst = new List<ViewParam>();
        public List<PatientDTO> GetPatient()
        {
            List<PatientDTO> patient = new List<PatientDTO>();
            DT = DM.Get("select top 2000 ptmast_kid,convert(nvarchar(20),ptmast_date,105) as ptmast_date ,ptitle_ename,ptmast_ename,ptmast_sex,gender,Age, convert(nvarchar(20),ptmast_dob,105) as  ptmast_dob, pct_ename from View_h_ptmast");
            if (DT !=null && DT.Rows.Count>0)
            {
                patient = DT.AsEnumerable().Select(x => new PatientDTO
                {
                    Age = x.Field<string>("Age"),
                    gender = x.Field<string>("gender"),
                    pct_ename = x.Field<string>("pct_ename"),
                    ptitle_ename= x.Field<string>("ptitle_ename"),
                    ptmast_date= x.Field<string>("ptmast_date"),
                    ptmast_dob= x.Field<string>("ptmast_dob"),
                    ptmast_ename= x.Field<string>("ptmast_ename"),
                    ptmast_kid= x.Field<int>("ptmast_kid"),
                    ptmast_sex= x.Field<string>("ptmast_sex"),

                }).ToList();
            }
            return patient;

        }
    }
}
