﻿using System;
using System.Collections.Generic;
using System.Text;
using HMSAPI.Core;
using HMSAPI.DTO;

namespace HMSAPI
{
    public interface  IUserCallService
    {
        LoginResponseDto UserLogin(UserLoginDto obj);
        TResponse RegisterUser(UserCallDto Obj);
        TResponse userLogSave(UserCallLogDto obj);
    }
}
