﻿using HMSAPI.Core;
using HMSAPI.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;

namespace HMSAPI.SERVICES
{

    public class UserCallService : IUserCallService
    {
        TResponse objTResponse = new TResponse();

        DataManager dm = new DataManager();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        List<ViewParam> _lst = new List<ViewParam>();
        public LoginResponseDto UserLogin(UserLoginDto obj)
        {
            LoginResponseDto login = new LoginResponseDto();
            if (!string.IsNullOrEmpty(obj.MobileNo))
            {
                DataTable dt = dm.Get("select count(*) FROM c_usr where   usr_Mobile='" + obj.MobileNo + "' and usr_status=1 ");
                if (dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][0].ToString()) > 0)
                {
                    string authToken = Guid.NewGuid().ToString();
                    _lst.Add(new ViewParam() { Name = "@authToken", Value = authToken });
                    _lst.Add(new ViewParam() { Name = "@mobile", Value = obj.MobileNo });
                    _lst.Add(new ViewParam() { Name = "@MID", Value = obj.MId });
                    ds = dm.GetDataSet("C_App_LoginUser", _lst);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        login = ds.Tables[0].AsEnumerable().Select(x => new LoginResponseDto
                        {
                            ID = x.Field<int>("usr_kid"),
                            AutKey = x.Field<string>("usr_AutKey"),
                            EmailID = x.Field<string>("use_Email"),
                            MobileNo = x.Field<string>("usr_Mobile"),
                            Name = x.Field<string>("usr_Name")
                        }).FirstOrDefault();

                        login.Token = Cipher.Encrypt(JsonConvert.SerializeObject(login));
                    }
                    else
                    {
                        login.AutKey = "1";
                    }
                }
                else
                {
                    login.AutKey = "0";
                }
            }
            return login;
        }

        public TResponse RegisterUser(UserCallDto Obj)
        {
            _lst.Add(new ViewParam { Name = "@id", Value = Obj.ID });
            _lst.Add(new ViewParam { Name = "@Name", Value = Obj.Name });
            _lst.Add(new ViewParam { Name = "@Mobile", Value = Obj.MobileNo });
            _lst.Add(new ViewParam { Name = "@email", Value = Obj.EmailID });
            _lst.Add(new ViewParam { Name = "@status", Value = Obj.Status });
            ds = dm.GetDataSet("C_App_UserSave", _lst);
            if (ds != null && ds.Tables[0].Rows[0][0].ToString().Trim() == "1")
            {
                objTResponse.ResponseCode = (int)HttpStatusCode.OK;
                objTResponse.ResponseStatus = true;
                objTResponse.ResponseMessage = ResponseMessage.Save;
            }
            else if (ds != null && ds.Tables[0].Rows[0][0].ToString().Trim() == "2")
            {
                objTResponse.ResponseCode = (int)HttpStatusCode.OK;
                objTResponse.ResponseStatus = true;
                objTResponse.ResponseMessage = ResponseMessage.UserUpdate;
            }
            else if (ds != null && ds.Tables[0].Rows[0][0].ToString().Trim() == "3")
            {
                objTResponse.ResponseCode = (int)HttpStatusCode.NotAcceptable;
                objTResponse.ResponseStatus = false;
                objTResponse.ResponseMessage = ResponseMessage.MobileExist;
            }
            else
            {
                objTResponse.ResponseCode = (int)HttpStatusCode.InternalServerError;
                objTResponse.ResponseStatus = true;
                objTResponse.ResponseMessage = ResponseMessage.Error;
            }
            return objTResponse;
        }

        public TResponse userLogSave(UserCallLogDto obj)
        {
            _lst.Add(new ViewParam { Name = "@UserId", Value = obj.UserId });
            _lst.Add(new ViewParam { Name = "@Mobile", Value = obj.MobileNo });
            _lst.Add(new ViewParam { Name = "@Type", Value = obj.Type });
            _lst.Add(new ViewParam { Name = "@Duration", Value = obj.Duration });
            _lst.Add(new ViewParam { Name = "@SDate", Value = obj.SDate });
            _lst.Add(new ViewParam { Name = "@EDate", Value = obj.EDate });
            ds = dm.GetDataSet("C_App_UserLog_Save", _lst);
            if (ds != null && ds.Tables[0].Rows[0][0].ToString().Trim() == "1")
            {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                objTResponse.ResponseCode = (int)HttpStatusCode.OK;
                objTResponse.ResponseStatus = true;
                objTResponse.ResponseMessage = ResponseMessage.Save;
            }
            else if (ds != null && ds.Tables[0].Rows[0][0].ToString().Trim() == "2")
            {
                objTResponse.ResponseCode = (int)HttpStatusCode.OK;
                objTResponse.ResponseStatus = true;
                objTResponse.ResponseMessage = ResponseMessage.UserUpdate;
            }
            else
            {
                objTResponse.ResponseCode = (int)HttpStatusCode.InternalServerError;
                objTResponse.ResponseStatus = true;
                objTResponse.ResponseMessage = ResponseMessage.Error;
            }
            return objTResponse;
        }
    }
}
