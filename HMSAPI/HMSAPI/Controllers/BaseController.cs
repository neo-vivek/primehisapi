﻿using HMSAPI.Core;
using HMSAPI.DTO;
using HMSAPI.SERVICES;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace HMSAPI.Controllers
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        public LoginResponseDto CurrentUser { get { return (LoginResponseDto)HttpContext.Items["CurrentUser"]; } }
    }
}

       