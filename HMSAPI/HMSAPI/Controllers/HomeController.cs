﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HMSAPI.Service;
using HMSAPI.Core;

namespace HMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private IPatient patient;
        public HomeController(IPatient patient)
        {
            this.patient = patient;
        }

        TResponse response = new TResponse();

        [HttpGet]
        [LoginAPIValidation]
        public TResponse GetPatient()
        {
            var model = patient.GetPatient();
            if (model != null)
            {
                response.ResponseCode = ResponseCode.OK;
                response.ResponseStatus = true;
                response.ResponsePacket = model;
            }
            else
            {
                response.ResponseCode = ResponseCode.OK;
                response.ResponseStatus = false;
                response.ResponsePacket = model;
            }
            return response;
        }
    }
}
