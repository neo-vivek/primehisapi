﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Net;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net.Http;
using HMSAPI.Core;
using HMSAPI.DTO;
using System.Web.Http.Controllers;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMSAPI.Core
{
    public class APIValidation : ActionFilterAttribute
    {
        private const string AuthenticationHeaderName = "Authorization";

        private bool IsAuthenticated(ActionExecutingContext actionContext)
        {
            var authenticationString = actionContext.HttpContext.Request.Headers[AuthenticationHeaderName];
            var msg = true;
            if (string.IsNullOrEmpty(authenticationString))
                msg = false;

            return msg;
        }


        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            bool isAuthenticated = IsAuthenticated(actionContext);
            if (!isAuthenticated)
            {

                actionContext.Result = new JsonResult(new TResponse
                {
                    ResponseCode = (int)HttpStatusCode.Unauthorized,
                    ResponseMessage = ResponseMessage.Unauthorised,
                    ResponseStatus = false,
                });

            }
        }
    }
}