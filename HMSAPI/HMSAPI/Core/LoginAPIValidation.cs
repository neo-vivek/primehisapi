﻿using HMSAPI.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using HMSAPI.Service;

namespace HMSAPI.Core
{
    public class LoginAPIValidation : ActionFilterAttribute
    {
        //GET: APIValidation
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            var Header = actionContext.HttpContext.Request.Headers;
            bool AuthenticatedUser = true;
            if (Header.ContainsKey("AuthPswd") && Header.ContainsKey("CompName"))
            {
                string AuthNumber = Header["AuthPswd"];
                string CompName = Header["CompName"];
                var response = new AuthenticationService().Authentication(CompName);
                if (response != null && !string.IsNullOrEmpty(response.AuthPswd) && PasswordEncryption.CreatePasswordHash(response.AuthPswd, response.Key, "MD5") == AuthNumber)
                {
                    AuthenticatedUser = true;
                }
                else
                {
                    AuthenticatedUser = false;
                }
            }
            else
            {
                AuthenticatedUser = false;
            }
            if (!AuthenticatedUser)
            {
                actionContext.Result = new JsonResult(new TResponse
                {
                    ResponseCode = (int)HttpStatusCode.Unauthorized,
                    ResponseMessage = ResponseMessage.Unauthorised,
                    ResponseStatus = false,
                });

            }
            base.OnActionExecuting(actionContext);
        }
    }
}


